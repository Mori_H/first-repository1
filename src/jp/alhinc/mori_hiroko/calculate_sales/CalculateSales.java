package jp.alhinc.mori_hiroko.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.filechooser.FileFilter;

public class CalculateSales {
    public static void main(String[] args) {

    	BufferedReader br = null;  //ｂｒにnull

        try {
            // 1.ファイルの場所、名前を指定
            File file = new File(args[0],"branch.lst");
            FileReader fr = new FileReader(file);
             br = new BufferedReader(fr); //生成したｆｒ情報入りBRをｂｒに。

           
             // shopデータを格納するリスト
//            List<String>shopdata = new ArrayList<>();
             //1行で読み込み、nullじゃないときに
            String line;
            while ((line = br.readLine()) != null) {
            	 // コンマで行を分割
                String[] code = line.split(",");
                
//                System.out.println(code[0]+code[1]); 001札幌支店となるコード　いらない
                
                HashMap<String,String>map = new HashMap<>();
                map.put(code[0],code[1]);  //キーと文字列の結びつき
                System.out.println(code[0] + "," + code[1]);
            }
        
        }
            //エラー発生
            catch(IOException e) {
            	System.out.println("エラーが発生しました。");

            }finally {  //最後にクローズ
            	if(br != null) {  //もしｂｒに参照が入ってない状態じゃないならば
            		try {
            			br.close();
            		}catch(IOException e) {
            			System.out.println("closeできませんでした。");
            		}
            	}
            }
    


    

//ここで一旦2-1完了　クラスは閉じてない & publicも。

    //集計　必要なければ↑の中へ
//    static void syuukei(String[] args) {
//
//    	BufferedReader br = null;  //ｂｒにヌル

//    	try {
    	File dir = new File(args[0]);	//ファイルがあるディレクトリ指定
 
    	
    	File[] files = dir.listFiles(new FileFilter());
    	for(File file : files) { //dirの中のファイルリストがファイルズに。
    		System.out.println(file.toString());
    	}
    	
    	class FileFilter implements FilenameFilter{
    //		FilenameFilter filter = new FilenameFilter() {
    		public boolean accept(File file, String name){ //accept内にフィルタする条件

				if (name.endsWith("rcd") && (name.matches("^[0-9]{8}"))){// 拡張子と8桁を指定
					System.out.println("file,name");
					return true;
				}else{
				
				return false;
    		}
    		}
    }
    
    		

//ato				FileReader fr = new FileReader(file);
//				br = new BufferedReader(fr); //生成したｆｒ情報入りBRをｂｒに。
//
//			String line;
//			while ((line = br.readLine()) != null) {
//				System.out.println(line);


////					return true;
////				}else{
////					return false;
//
//					//"crd"が含まれるフィルタを作成する
//					File[] files = new File("rcd").listFiles(filter);
//
//					//結果を出力する
//					for(int i=00000000; i<files.length; ++i){
//						System.out.println(files[i]);
				}
}




    	/*
    		File file = new File(args[0],"");
    		FileReader fr = new FileReader(file);
            br = new BufferedReader(fr); //生成したｆｒ情報入りBRをｂｒに。

            //1行で読み込み、ヌルじゃないときに出力
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            catch(IOException e) {
            	System.out.println("エラーが発生しました。");

            }finally {  //最後にクローズ
            	if(br != null) {  //もしｂｒに参照が入ってない状態じゃないならば
            		try {
            			br.close();
            		}catch(IOException e) {
            			System.out.println("closeできませんでした。");
            		}
            	}
            }
    		}
    		}
    	}
    }




            /*

            // 2.ファイルが存在しない場合に例外
            if (!file.exists()) {
                System.out.print("支店定義ファイルが存在しません");
                return;
            }

         // BufferedReaderクラスのreadLineメソッドを使って1行ずつ読み込み表示する
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                System.out.println(data);
            }


   /*↑のコードがヌル返してくる         if (!line.matches("^[0-9]{3}") ) {
                System.out.println("ファイルフォーマットが不正です");
                bufferedReader.close();
                return;
            } */


            // 最後にファイルを閉じてリソースを開放する
  /*          bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}



/*if (!datas[0].matches("^[0-9]{3}$") || datas.length != 2) {
    System.out.println("ファイルフォーマットが不正です");
    return;




































/*import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class CalculateSales {
	public static void main(String[] args) {


		try
			File fire = new File(args[0], "branch.lst");	//支店定義ファイル読み込み
			FileReader fr = new FileReader(fire);
			BufferedReader br = new BufferedReader(fr);
			br.close();



            //ファイルの読み込み
            int ch;
            while((ch = fr.read()) != -1){
                System.out.print((char)ch);


			FileReader fileReader = null;

            }




		}catch(FileNotFoundException e){					//ファイルが存在しない場合（支店）
			System.out.println("支店定義のファイルが存在しません。");
			return;

		}catch(IllegalStateException e) {					//不正なファイルの場合（支店）
			System.out.println("支店定義のファイルのフォーマットが不正です。");
			return;

		}finally {

		}
		}
	}


/*


		try{
			Fire fire = new File(args[1], " .rcd");	//	集計、売上ファイル、ファイル名連番
			FileReader filereader = new FileReader(fire);

		}catch() {			//売上ファイルが連番になってない


		}catch() {			//合計金額が10桁以上

		}catch() {			//支店に該当がなかった場合

		}catch() {			//売上ファイルの中身が3行以上ある場合

		}catch() {			//処理内容に記載のないエラーメッセージ


		}finally {


		}


	}
}
*/

